import React from 'react'
let receta = {
    nombre: 'Pizza',
    ingredientes: ['Tomate', 'Queso', 'Jamón cocido'],
    calorias: 400
}

// props: argumentos que se pasan al componente funcional
// Los props son constantes para el componente
// No puedes hacer esto:
// props.nombre = "Otra cosa"
const MiCompoFuncional = (props) => {    
    // <></> equivale a React.fragment

    // React.useState() toma como único argumento el
    // estado inicial y devuelve un arreglo de 2 elementos
    // El primer elemento es la referencia al estado
    // El segundo es la función que permitirá cambiar el estado
    const [estado, cambiarEstado] = React.useState(0)

    // No puedo hacer esto porque el estado
    // Es una variable inmutable
    // Cambiarlo directamente es una acción ilegal
    //estado = true

    // La forma correcta de cambiar el estado es
    // cambiarEstado(true)

    // Analogía de estado y cambiarEstado
    // (no es como funciona internamente pero se parece)
    /*
        let estado = 0
        ...
        const cambiarEstado (argumento) => {
            estado = argumento
        }
    */

    const presionarBoton = () => {
        cambiarEstado(estado + 1)
    }

    return (
        <>
            <p>{estado.toString()}</p>
            <button onClick={presionarBoton}>Cambiar estado</button>
        </>
    )
}

// Export default significa
// que no afecta el nombre con que
// importemos este componente
// desde otro archivo

// Por ejemplo
// En App.js:
// import MiCompoConOtroNombre from './components/MiCompoFuncional'
export default MiCompoFuncional