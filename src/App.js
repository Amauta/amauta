import React from 'react';
import logo from './assets/images/logo.svg';
import './assets/css/App.css';
// watch [...] ERNOSPC

// Importar componentes
import MiCompo from './components/MiCompo'
import MiCompoFuncional from './components/MiCompoFuncional'
import MiCompoFun2 from './components/MiCompoFun2'
import Contador from './components/Contador'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Hola a Neurona 7
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>

        <section className="componentes">
        {/*<MiCompo>*/}
        {/*<MiCompoFuncional nombre="Manuel"/>*/}
        
        {/*<MiCompoFun2 nombre="Ricardo"/>*/}

        <MiCompoFun2/>

        </section>

      </header>
    </div>

    
  );
}

export default App;
